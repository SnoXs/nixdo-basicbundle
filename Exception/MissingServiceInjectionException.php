<?php

namespace Nixdo\BasicBundle\Exception;

/**
 * 
 */
class MissingServiceInjectionException extends \Exception {

    public function __construct($message, $class) {
        parent::__construct($message . " has not been set in " . $class);
    }

}
