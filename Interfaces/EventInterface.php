<?php

namespace Nixdo\BasicBundle\Interfaces;

interface EventInterface {

    public function getEventName();
}
