<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Nixdo\BasicBundle\Interfaces\EventInterface;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait DispatcherTrait {

    private $eventDispatcher = null;

    protected function dispatch(EventInterface $event) {
        if ($this->getEventDispatcher() !== null) {
            $this->getEventDispatcher()->dispatch($event->getEventName(), $event);
        } else {
            throw new MissingServiceInjectionException("Event dispatcher", get_class());
        }
        return $this;
    }

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
        return $this;
    }

    public function getEventDispatcher() {
        if ($this->eventDispatcher === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('event_dispatcher')) {
                    throw new \LogicException('The EventDispatcher is not registered in your application.');
                }
                $this->eventDispatcher = $this->container->get('event_dispatcher');
            } else if (method_exists($this, "getContainer")) {
                $this->eventDispatcher = $this->getContainer()->get('event_dispatcher');
            }
        }
        return $this->eventDispatcher;
    }

}
