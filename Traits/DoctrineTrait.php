<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait DoctrineTrait {

    private $doctrine = null;
    private $entityManager = null;

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        return $this;
    }

    public function clearEntityManager() {
        $this->entityManager = null;
        return $this;
    }

    public function getEntityManager() {
        return $this->entityManager;
    }

    protected function persist($object) {
        $this->getManager()->persist($object);
        return $this;
    }

    protected function remove($object) {
        $this->getManager()->remove($object);
        return $this;
    }

    protected function flush($data = null) {
        if ($data === null) {
            $this->getManager()->flush();
        } else {
            $this->getManager()->flush($data);
        }
        return $this;
    }

    protected function clear() {
        $this->getManager()->clear();
        return $this;
    }

    protected function reset() {
        if ($this->getDoctrine() !== null) {
            $this->getDoctrine()->resetManager();
        } else {
            throw new MissingServiceInjectionException("Doctrine", get_class());
        }
        return $this;
    }

    public function getRepository($name) {
        return $this->getManager()->getRepository($name);
    }

    protected function getManager($manager = null) {
        if ($this->getDoctrine() !== null) {
            if ($manager !== null) {
                return $this->getDoctrine()->getManager($manager);
            } else if ($this->entityManager !== null) {
                return $this->getDoctrine()->getManager($this->entityManager);
            } else {
                return $this->getDoctrine()->getManager();
            }
        } else {
            throw new MissingServiceInjectionException("Doctrine", get_class());
        }
    }

    public function setDoctrine(RegistryInterface $doctrine) {
        $this->doctrine = $doctrine;
        return $this;
    }

    public function getDoctrine() {
        if ($this->doctrine === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('doctrine')) {
                    throw new \LogicException('The DoctrineBundle is not registered in your application.');
                }
                $this->doctrine = $this->container->get('doctrine');
            } else if (method_exists($this, "getContainer")) {
                $this->doctrine = $this->getContainer()->get('doctrine');
            }
        }
        return $this->doctrine;
    }

}
