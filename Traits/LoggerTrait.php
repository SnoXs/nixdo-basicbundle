<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Bridge\Monolog\Logger;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait LoggerTrait {

    protected $logger = null;

    public function getLastError($exception = null) {
        $error = \error_get_last();
        return array("lasterror" => $error, "exception" => $exception);
    }

    public function debug($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->debug($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function info($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->info($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function notice($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->notice($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function warning($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->warning($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function error($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->error($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function critical($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->critical($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function alert($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->alert($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function emergency($message, array $context = array()) {
        if ($this->getLogger() !== null) {
            $this->getLogger()->emergency($message, $context);
        } else {
            throw new MissingServiceInjectionException("Logger", get_class());
        }
    }

    public function setLogger(Logger $logger) {
        $this->logger = $logger;
        return $this;
    }

    public function getLogger() {
        if ($this->logger === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('logger')) {
                    throw new \LogicException('The Logger service is not registered in your application.');
                }
                $this->logger = $this->container->get('logger');
            } else if (method_exists($this, "getContainer")) {
                $this->logger = $this->getContainer()->get('logger');
            }
        }
        return $this->logger;
    }

}
