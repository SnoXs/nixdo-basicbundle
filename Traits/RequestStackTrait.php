<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Component\HttpFoundation\RequestStack;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait RequestStackTrait {

    protected $requestStack = null;

    protected function getCurrentRequest() {
        if ($this->requestStack !== null) {
            return $this->requestStack->getCurrentRequest();
        } else {
            throw new MissingServiceInjectionException("Request Stack", get_class());
        }
    }

    public function setRequestStack(RequestStack $requestStack) {
        $this->requestStack = $requestStack;
        return $this;
    }

    public function getRequestStack() {
        return $this->requestStack;
    }

}
