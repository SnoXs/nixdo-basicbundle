<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait RouterTrait {

    protected $router = null;

    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH) {
        if ($this->getRouter() !== null) {
            return $this->getRouter()->generate($route, $parameters, $referenceType);
        } else {
            throw new MissingServiceInjectionException("Router", get_class());
        }
    }

    public function setRouter(RouterInterface $router) {
        $this->router = $router;
        return $this;
    }

    public function getRouter() {
        if ($this->router === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('router')) {
                    throw new \LogicException('The Router service is not registered in your application.');
                }
                $this->router = $this->container->get('router');
            } else if (method_exists($this, "getContainer")) {
                $this->router = $this->getContainer()->get('router');
            }
        }
        return $this->router;
    }

}
