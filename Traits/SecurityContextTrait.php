<?php

namespace Nixdo\BasicBundle\Traits;

use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;
use Symfony\Component\Security\Core\SecurityContextInterface;

trait SecurityContextTrait {

    protected $securityContext = null;

    public function getCurrentUser() {
        if ($this->securityContext !== null) {
            $token = $this->securityContext->getToken();
            if ($token === null) {
                return null;
            } else {
                return $token->getUser();
            }
        } else {
            throw new MissingServiceInjectionException("Security Context", get_class());
        }
    }

    public function setSecurityContext(SecurityContextInterface $securityContext) {
        $this->securityContext = $securityContext;
        return $this;
    }

    public function getSecurityContext() {
        return $this->securityContext;
    }

}
