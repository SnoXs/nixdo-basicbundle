<?php

namespace Nixdo\BasicBundle\Traits;

use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait SecurityTokenStorageTrait {

    protected $securityTokenStorage = null;
    protected $currentUser = null;

    public function setCurrentUser($user) {
        $this->currentUser = $user;
        return $this;
    }

    public function getCurrentUser() {
        if ($this->currentUser !== null) {
            return $this->currentUser;
        } else {
            if ($this->securityTokenStorage !== null) {
                $token = $this->securityTokenStorage->getToken();
                if ($token === null) {
                    return null;
                } else {
                    return $token->getUser();
                }
            } else {
                throw new MissingServiceInjectionException("Security Token Storage", get_class());
            }
        }
    }

    public function setSecurityTokenStorage(TokenStorageInterface $securityTokenStorage) {
        $this->securityTokenStorage = $securityTokenStorage;
        return $this;
    }

    public function getSecurityTokenStorage() {
        return $this->securityTokenStorage;
    }

}
