<?php

namespace Nixdo\BasicBundle\Traits;

use Symfony\Component\Translation\TranslatorInterface;
use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait TranslatorTrait {

    protected $translator = null;

    protected function setLocale($locale) {
        if ($this->getTranslator() !== null) {
            $this->getTranslator()->setLocale($locale);
        } else {
            throw new MissingServiceInjectionException("Translator", get_class());
        }
        return $this;
    }

    protected function getLocale() {
        if ($this->getTranslator() !== null) {
            return $this->getTranslator()->getLocale();
        } else {
            throw new MissingServiceInjectionException("Translator", get_class());
        }
    }

    public function trans($id, array $parameters = array(), $domain = 'messages', $locale = null) {
        if ($this->getTranslator() !== null) {
            return $this->getTranslator()->trans($id, $parameters, $domain, $locale);
        } else {
            throw new MissingServiceInjectionException("Translator", get_class());
        }
    }

    public function setTranslator(TranslatorInterface $translator) {
        $this->translator = $translator;
        return $this;
    }

    public function getTranslator() {
        if ($this->translator === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('translator')) {
                    throw new \LogicException('The Translator service is not registered in your application.');
                }
                $this->translator = $this->container->get('translator');
            } else if (method_exists($this, "getContainer")) {
                $this->translator = $this->getContainer()->get('translator');
            }
        }
        return $this->translator;
    }

}
