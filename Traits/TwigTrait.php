<?php

namespace Nixdo\BasicBundle\Traits;

use Nixdo\BasicBundle\Exception\MissingServiceInjectionException;

trait TwigTrait {

    protected $twig = null;
    protected $twigArray = array();

    protected function add($key, $value) {
        $this->twigArray[$key] = $value;
        return $this;
    }

    protected function render($name, array $context = array()) {
        if ($this->getTwig() !== null) {
            $template = $this->getTwig()->render($name, array_merge($context, $this->twigArray));
            $this->twigArray = array();
        } else {
            throw new MissingServiceInjectionException("Twig", get_class());
        }
        return $template;
    }

    public function setTwig(\Twig_Environment $twig) {
        $this->twig = $twig;
        return $this;
    }

    public function getTwig() {
        if ($this->twig === null) {
            if (property_exists($this, 'container')) {
                if (!$this->container->has('twig')) {
                    throw new \LogicException('The Twig Bundle is not registered in your application.');
                }
                $this->twig = $this->container->get('twig');
            } else if (method_exists($this, "getContainer")) {
                $this->twig = $this->getContainer()->get('twig');
            }
        }
        return $this->twig;
    }

}
